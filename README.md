# Erran Carey - Competencies and Values Alignment

See also [Development Department Career Framework: Principal](https://handbook.gitlab.com/handbook/engineering/careers/matrix/development/principal/).

> ## Principal Leadership Competencies
> 
> ### Engineering
> 
> A principal engineer generally operates at an organizational (sub-department or stage, for example) level scope, serving as their organization’s technical lead and connecting their organization to other parts of GitLab.
> 
> <details><summary>Click to expand Engineering Leadership Competencies</summary>
> 
> - Helps team members of all levels understand their organization’s domain and technology through clear and effective communication.
> - Makes responsible decisions and evaluates tradeoffs impacting their organization and various aspects of the business.
> - Exhibits a deep understanding of their organization’s features, how customers use them, and how they fit into the larger business.
> - Improves processes at the organization level.
> - Recognizes good solutions to complex problems impacting multiple domains and clearly explains their merits to relevant stakeholders.
> - Unblocks and enables team members and counterparts across multiple teams.
> - Participates in processes which address complex technical challenges, such as the Architecture Design Process, as a DRI and as a coach.
> - Advises on resourcing requirements and provides feedb
> ack on promotions into technical leadership roles.
> 
> 
> ### Development
> 
> - Able to deliver results despite starting from unclear requirements.
> 
> </details>
> 
> ## Principal Technical Competencies
> 
> ### Engineering
> 
> <details><summary>Click to expand engineering technical competencies</summary>
> 
> Has a broad skill-set with in-depth expertise in several areas.
> Blends technical, product and design strategy, helping their organization to be more productive.
> Represents their organization as a domain expert when interfacing with other teams and departments.
> Reaches beyond immediate needs, independently considering the longer-term, and is always looking beyond for ways to have a broad impact.
> Addresses cross-team challenges related to customers, quality and security.
> Able to take on cross-team complex requirements and decompose them into a proposal of small deliverables.
> Engages in processes, such as the Architecture Design Process, to contribute to their organization’s most challenging technical initiatives.
> 
> </details>
> 
> ### Development
> 
> <details><summary>Click to expand Development technical competencies</summary>
> 
> - The Principal Engineer role acts as the individual equivalent of a Senior Engineering Manager, Development.
> - Extends that of the Staff Frontend Engineer or the Staff Backend Engineer responsibilities
> - Collaborates and makes proposals across several teams on their engineering work, and helps their team members make informed decisions in alignment with the sub-department strategic plans.
> - Exposes technology and organizational needs across their sub-department.
> - Teach, mentor, grow, and provide advice to other domain experts, individual contributors, across several teams in their sub-department.
> - Play a central role in technical, business, and organizational contributions affecting the sub-department/department.
> - Plan improvements and features with a 6 month view.
> - Solves technical problems of the highest scope, complexity, and ambiguity for their sub-department.
> - Interfaces with EMs and Senior management, and enables Staff Engineers to engage on department-level aspects of larger (sub-department wide) initiatives.
> - Looks for innovation opportunities between several teams with a willingness to experiment and to boldly confront problems of large complexity and scope.
> - Proposes initial technical implementations which support architectural changes that solve scaling and performance problems.
> - Ensures that OKR level goals are aligned across several teams in their sub-department.
> - Guides conversations to remove blockers and encourage collaboration across teams.
> - Provides a point of escalation for sub-department teams facing complex technical challenges.
> - Attain a measurable impact on the work of sub-department teams.
> - Interact with customers and other external stakeholders as a consultant and spokesperson for the work of your sub-department.
> - Exposes the work of the sub department and their business impact internally.
> - Knowledgeable in all specialities practiced within the department or sub-department
> 
> </details>
> 
> ## Principal Values Alignment
> 
> ### GitLab
> 
> <details><summary>Click to expand GitLab Values</summary>
> 
> - Adhere to the Collaboration Competencies for this job grade.
> - Adhere to the Results Competencies for this job grade.
> - Adhere to the Efficiency Competencies for this job grade.
> - Adhere to the Diversity, Inclusion & Belonging Competencies for this job grade.
> - Adhere to the Iteration Competencies for this job grade.
> - Adhere to the Transparency Competencies for this job grade.
> 
> </details>
> 
> ## Development
> 
> <details><summary>Click to expand Development Values</summary>
> 
> - Regularly engages with teams other than their own.
> - Identifies situations where collaboration between teams will yield good results.
> - Iteratively works towards results on cross-team projects without over-analysis.
> - Works with the Senior Engineering Manager to assign work to the team.
> - Is able to take a long term goal and turn this into small actionable steps that can be implemented in an iterative way.
> - Identifies and prevents decisions that are not “two-way door decisions”.
> - Champions a no-blame culture and encourages learning from mistakes.
> 
> </details>
